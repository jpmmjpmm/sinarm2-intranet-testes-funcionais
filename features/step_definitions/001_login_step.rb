Dado("que estou na pagina de login do SINARM") do
  @login_page = LoginPage.new
  @login_page.load
end

Quando("eu informo usuario {string} e senha {string}") do |usuario, senha|
  @login_page.logar(usuario, senha)
end

Entao("sou autenticado com {string}") do |usuario_logado|
  expect(@login_page.usuario_logado.text).to include(usuario_logado)
end
